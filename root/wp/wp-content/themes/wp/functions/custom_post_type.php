<?php
/* ===============================================================================
  カスタム投稿設定ファイル
=============================================================================== */
function my_custompost() {

$cp["label"]   = "新着情報";
$cp["post_type"] = "news";

register_post_type($cp["post_type"], array(
'label' => $cp["label"],
'menu_icon'   => 'dashicons-admin-post',
'menu_position' => 5,
'has_archive' => true,
'description' => '',
'public' => true,
'show_ui' => true,
'show_in_menu' => true,
'capability_type' => 'post',
'hierarchical' => false,
'rewrite' => array( 'slug' => 'news', 'with_front' => false ),
'query_var' => true,
'supports' => array('title','editor'),
'labels' => array (
'name' => $cp["label"],
'singular_name' => '',
'menu_name' => $cp["label"],
'add_new' => '新規追加',
'add_new_item' => $cp["label"].'を新規追加',
'edit' => 'Edit',
'edit_item' => $cp["label"].'を編集',
'new_item' => 'New '.$cp["label"],
'view' => $cp["label"].'を表示',
'view_item' => $cp["label"].'を表示',
'search_items' => $cp["label"].'を検索',
'not_found' => $cp["label"].'はありません',
'not_found_in_trash' => 'ゴミ箱は空です',
'parent' => 'Parent '.$cp["label"],
),) );
register_taxonomy( 'news_cat', 'news', array(
  'label'        => 'カテゴリー',
  'public' => true,
  'hierarchical' => true,
  'show_admin_column' => true,
  'query_var'         => true,
  'rewrite'           => array( 'slug' => 'news' ),
  'show_ui'           => true,
));
}
add_action( 'init', 'my_custompost', 0 );
