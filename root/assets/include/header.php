<!doctype html>
<html lang="ja" id="pagetop">
<head>

<?php ////////////////////////////////////////////////////
// Meta
//////////////////////////////////////////////////////////?>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include('meta.php');?>
<link href="/assets/css/style.min.css" rel="stylesheet">
<script src="/assets/js/jquery-3.3.1.min.js"></script>
<script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
<?php ////////////////////////////////////////////////////
// WP head
//////////////////////////////////////////////////////////?>
<?php if (!$pageid) {
    // wp_head();
} ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5NGLV7L');</script>
<!-- End Google Tag Manager -->
</head>

<?php ////////////////////////////////////////////////////
// Body Class
//////////////////////////////////////////////////////////?>
<?php if ($pageid) {
    ?>

	<body id="pagetop" class="page-<?php echo $pageid; ?>">

<?php
} else {
        if (is_page('top') || is_home()) {
            $class="page-wptop";
        } elseif (is_page()) {
            $class="page-".get_page($page_id)->post_name;
        } elseif (is_archive()) {
            $class="archive-".get_post_type();
        } elseif (is_single()) {
            $class="single";
            $class.=" single-".get_post_type();
            $class.=" single-".$post->post_name;
        }
        $class_ie="";

        $ua = $_SERVER['HTTP_USER_AGENT'];
        if (strstr($ua, 'Trident') || strstr($ua, 'MSIE')) {
            $class_ie=" ie";
        } ?>

	<body id="pagetop" class="<?php echo $class; ?><?php echo $class_ie; ?>">

<?php
    } ?>
<!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5NGLV7L"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php ////////////////////////////////////////////////////
// Header
//////////////////////////////////////////////////////////?>
