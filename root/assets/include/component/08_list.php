<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1</div>
<div class="l-container">
    <ul class="c-list1">
        <li class="c-list1__card ">
            <a href="" class="c-arrow">
                <div class="c-list1__img">
                    <img src="/recruit/assets/img/index/100.jpg" alt="">
                </div>
                <div class="c-list1__text">
                    <h3>代表メッセージ</h3>
                </div>
            </a>
        </li>
        <li class="c-list1__card">
            <a href="" class="c-arrow">
                <div class="c-list1__img">
                    <img src="/recruit/assets/img/index/101.jpg" alt="">
                </div>
                <div class="c-list1__text"> <h3>事業紹介 </h3></div>
            </a>
        </li>
        <li class="c-list1__card ">
            <a href="" class="c-arrow">
                <div class="c-list1__img">
                    <img src="/recruit/assets/img/index/102.jpg" alt="">
                </div>
                <div class="c-list1__text"><h3>JXTGグループの強み</h3></div>
            </a>
        </li>
        <li class="c-list1__card ">
            <a href="" class="c-arrow">
                <div class="c-list1__img">
                    <img src="/recruit/assets/img/index/103.jpg" alt="">
                </div>
                <div class="c-list1__text"><h3>データで見る<br>中央石油販売</h3></div>
            </a>
        </li>
    </ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1--col3</div>
<div class="l-container">
<ul class="c-list1 c-list1--col3">
        <li class="c-list1__card ">
            <a href="" class="c-arrow">
                <div class="c-list1__img">
                    <img src="/recruit/assets/img/info/101.jpg" alt="">
                </div>
                <div class="c-list1__text">
                    <h3>新卒採用</h3>
                </div>
            </a>
        </li>
        <li class="c-list1__card">
            <a href="" class="c-arrow">
                <div class="c-list1__img">
                    <img src="/recruit/assets/img/info/102.jpg" alt="">
                </div>
                <div class="c-list1__text"> <h3>中途採用 </h3></div>
            </a>
        </li>
        <li class="c-list1__card ">
            <a href="" class="c-arrow">
                <div class="c-list1__img">
                    <img src="/recruit/assets/img/info/103.jpg" alt="">
                </div>
                <div class="c-list1__text"><h3>アルバイト・パート採用 </h3></div>
            </a>
        </li>
    </ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1--col3height</div>
<div class="l-container">
    <ul class="c-list1 c-list1--col3height">
        <li class="c-list1__card ">
            <a href="" class="c-arrow">
                <div class="c-list1__img">
                    <img src="/recruit/assets/img/info-newraduates-requirements/101.jpg" alt="">
                </div>
                <div class="c-list1__text">
                    <h3>採用メッセージ／求める人物像 </h3>
                </div>
            </a>
        </li>
        <li class="c-list1__card">
            <a href="" class="c-arrow">
                <div class="c-list1__img">
                    <img src="/recruit/assets/img/info-newraduates-requirements/102.jpg" alt="">
                </div>
                <div class="c-list1__text"> <h3>募集要項／エントリー</h3></div>
            </a>
        </li>
        <li class="c-list1__card">
            <a href="" class="c-arrow">
                <div class="c-list1__img">
                    <img src="/recruit/assets/img/info-newraduates-requirements/103.jpg" alt="">
                </div>
                <div class="c-list1__text"><h3>よくあるご質問</h3></div>
            </a>
        </li>
    </ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1--col2</div>
<div class="l-container">
<ul class="c-list1 c-list1--col2">
    <li class="c-list1__card">
        <a href="" class="c-arrow">
            <div class="c-list1__img">
                <img src="/recruit/assets/img/culture-career-growth/104.jpg" alt="">
            </div>
            <div class="c-list1__text">
                <h3>キャリアステップ／教育・研修</h3>
            </div>
        </a>
    </li>
    <li class="c-list1__card">
        <a href="" class="c-arrow">
            <div class="c-list1__img">
                <img src="/recruit/assets/img/people-job/102.jpg" alt="">
            </div>
            <div class="c-list1__text">
                <h3>福利厚生／制度 </h3>
            </div>
        </a>
    </li>
</ul>
</div>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1--col2</div>
<div class="l-container">
    <ul class="c-list1 c-list1--col2">
        <li class="c-list1__card">
            <a href="" class="c-arrow">
                <div class="c-list1__img">
                    <img src="/recruit/assets/img/index/107.jpg" alt="">
                </div>
                <div class="c-list1__text">
                    <h3>ロードサイドビジネスを知る</h3>
                    <p>Roadside Business</p>
                </div>
                <div class="u-coming">
                    <p> Coming Soon<p>
                </div>
            </a>
        </li>
        <li class="c-list1__card">
            <a href="" class="c-arrow">
                <div class="c-list1__img">
                    <img src="/recruit/assets/img/index/108.jpg" alt="">
                </div>
                <div class="c-list1__text">
                    <h3>カーライフビジネスを知る</h3>
                    <p>Roadside Business</p>
                </div>
            </a>
        </li>
    </ul>
</div>



<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list2</div>
<div class="l-container">
    <ul class="c-list2">
        <li class="c-list2__card">
            <a href="" class="c-arrow">
                <div class="c-list2__img">
                    <img src="/recruit/assets/img/index/104.jpg" alt="">
                </div>
                <div class="c-list2__text">
                    <h3>山田 太郎</h3>
                    <span>Yamada Taro</span>
                    <p>2013年度入社 工学部卒<br>テリトリーマネージャー</p>
                </div>
            </a>
        </li>
        <li class="c-list2__card">
            <a href="" class="c-arrow">
                <div class="c-list2__img">
                    <img src="/recruit/assets/img/index/105.jpg" alt="">
                </div>
                <div class="c-list2__text">
                    <h3>山田 太郎</h3>
                    <span>Yamada Taro</span>
                    <p>2013年度入社 工学部卒<br>テリトリーマネージャー</p>
                </div>
            </a>
        </li>
        <li class="c-list2__card">
            <a href="" class="c-arrow">
                <div class="c-list2__img">
                    <img src="/recruit/assets/img/index/104.jpg" alt="">
                </div>
                <div class="c-list2__text">
                    <h3>山田 太郎</h3>
                    <span>Yamada Taro</span>
                    <p>2013年度入社 工学部卒<br>テリトリーマネージャー</p>
                </div>
            </a>
        </li>
        <li class="c-list2__card">
            <a href="" class="c-arrow">
                <div class="c-list2__img">
                    <img src="/recruit/assets/img/index/105.jpg" alt="">
                </div>
                <div class="c-list2__text">
                    <h3>山田 太郎</h3>
                    <span>Yamada Taro</span>
                    <p>2013年度入社 工学部卒<br>テリトリーマネージャー</p>
                </div>
            </a>
        </li>
        <li class="c-list2__card">
            <a href="" class="c-arrow">
                <div class="c-list2__img">
                    <img src="/recruit/assets/img/index/104.jpg" alt="">
                </div>
                <div class="c-list2__text">
                    <h3>山田 太郎</h3>
                    <span>Yamada Taro</span>
                    <p>2013年度入社 工学部卒<br>テリトリーマネージャー</p>
                </div>
            </a>
        </li>
        <li class="c-list2__card">
            <a href="" class="c-arrow">
                <div class="c-list2__img">
                    <img src="/recruit/assets/img/index/105.jpg" alt="">
                </div>
                <div class="c-list2__text">
                    <h3>山田 太郎</h3>
                    <span>Yamada Taro</span>
                    <p>2013年度入社 工学部卒<br>テリトリーマネージャー</p>
                </div>
            </a>
        </li>
        <li class="c-list2__card">
            <a href="" class="c-arrow">
                <div class="c-list2__img">
                    <img src="/recruit/assets/img/index/104.jpg" alt="">
                </div>
                <div class="c-list2__text">
                    <h3>山田 太郎</h3>
                    <span>Yamada Taro</span>
                    <p>2013年度入社 工学部卒<br>テリトリーマネージャー</p>
                </div>
            </a>
        </li>
        <li class="c-list2__card">
            <a href="" class="c-arrow">
                <div class="c-list2__img">
                    <img src="/recruit/assets/img/index/105.jpg" alt="">
                </div>
                <div class="c-list2__text">
                    <h3>山田 太郎</h3>
                    <span>Yamada Taro</span>
                    <p>2013年度入社 工学部卒<br>テリトリーマネージャー</p>
                </div>
            </a>
        </li>
    </ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list2--col3</div>
<div class="l-container">
<ul class="c-list2 c-list2--col3">
    <li class="c-list2__card">
        <a href="" class="c-arrow">
            <div class="c-list2__img">
                <img src="/recruit/assets/img/people-index/101.jpg" alt="">
            </div>
            <div class="c-title3 c-title3--small">
                <h3 class="c-title3__main">最初の1分に <span>情熱を。</span></h3>
            </div>
            <div class="c-list2__text">
                <h3>山田 太郎</h3>
                <span>Yamada Taro</span>
                <p>テリトリーマネージャー<br>2013年度入社 工学部卒</p>
            </div>
        </a>
    </li>
    <li class="c-list2__card">
        <a href="" class="c-arrow">
            <div class="c-list2__img">
                <img src="/recruit/assets/img/people-index/102.jpg" alt="">
            </div>
            <div class="c-title3 c-title3--small">
                <h3 class="c-title3__main">最初の1分に <span>情熱を。</span></h3>
            </div>
            <div class="c-list2__text">
                <h3>山田 太郎</h3>
                <span>Yamada Taro</span>
                <p>テリトリーマネージャー<br>2013年度入社 工学部卒</p>
            </div>
        </a>
    </li>
    <li class="c-list2__card">
        <a href="" class="c-arrow">
            <div class="c-list2__img">
                <img src="/recruit/assets/img/people-index/101.jpg" alt="">
            </div>
            <div class="c-title3 c-title3--small">
                <h3 class="c-title3__main">最初の1分に <span>情熱を。</span></h3>
            </div>
            <div class="c-list2__text">
                <h3>山田 太郎</h3>
                <span>Yamada Taro</span>
                <p>テリトリーマネージャー<br>2013年度入社 工学部卒</p>
            </div>
        </a>
    </li>
    <li class="c-list2__card">
        <a href="" class="c-arrow">
            <div class="c-list2__img">
                <img src="/recruit/assets/img/people-index/102.jpg" alt="">
            </div>
            <div class="c-title3 c-title3--small">
                <h3 class="c-title3__main">最初の1分に <span>情熱を。</span></h3>
            </div>
            <div class="c-list2__text">
                <h3>山田 太郎</h3>
                <span>Yamada Taro</span>
                <p>テリトリーマネージャー<br>2013年度入社 工学部卒</p>
            </div>
        </a>
    </li>
    <li class="c-list2__card">
        <a href="" class="c-arrow">
            <div class="c-list2__img">
                <img src="/recruit/assets/img/people-index/101.jpg" alt="">
            </div>
            <div class="c-title3 c-title3--small">
                <h3 class="c-title3__main">最初の1分に <span>情熱を。</span></h3>
            </div>
            <div class="c-list2__text">
                <h3>山田 太郎</h3>
                <span>Yamada Taro</span>
                <p>テリトリーマネージャー<br>2013年度入社 工学部卒</p>
            </div>
        </a>
    </li>
    <li class="c-list2__card">
        <a href="" class="c-arrow">
            <div class="c-list2__img">
                <img src="/recruit/assets/img/people-index/102.jpg" alt="">
            </div>
            <div class="c-title3 c-title3--small">
                <h3 class="c-title3__main">最初の1分に <span>情熱を。</span></h3>
            </div>
            <div class="c-list2__text">
                <h3>山田 太郎</h3>
                <span>Yamada Taro</span>
                <p>テリトリーマネージャー<br>2013年度入社 工学部卒</p>
            </div>
        </a>
    </li>
</ul>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list3</div>
<div class="l-container">
    <ul class="c-list3">
        <li>
            <a href="" class="c-arrow c-arrow--red">
                <div class="c-list3__img">
                    <img src="/recruit/assets/img/index/109.jpg" alt="">
                </div>
                <div class="c-list3__text">
                    <p><span>山田 太郎</span>2013年度入社 工学部卒<br>テリトリーマネージャー</p>
                    <p><img src="/recruit/assets/img/common/icon-multiply.png" alt=""></p>
                    <p><span>山田 花子</span>2013年度入社 工学部卒<br>テリトリーマネージャー</p>
                </div>
            </a>
        </li>
    </ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list4</div>
<div class="l-container">
    <ul class="c-list4">
        <li class="c-list4__card ">
            <a href="/recruit/company/info/philosophy.php" class="c-arrow">
                <div class="c-list4__img" style="background:url('/recruit/assets/img/company-index/101.jpg') center center; background-size:cover;">
                </div>
                <div class="c-list4__text"><h3>企業理念</h3></div>
            </a>
        </li>
        <li class="c-list4__card">
            <a href="/recruit/company/info/message.php" class="c-arrow">
                <div class="c-list4__img" style="background:url('/recruit/assets/img/company-index/102.jpg') center center; background-size:cover;">
                </div>
                <div class="c-list4__text"> <h3>代表<br class="sp-only"/>メッセージ</h3></div>
            </a>
        </li>
        <li class="c-list4__card ">
            <a href="/recruit/company/business/business.php" class="c-arrow">
                <div class="c-list4__img" style="background:url('/recruit/assets/img/company-index/103.jpg') center center; background-size:cover;">
                </div>
                <div class="c-list4__text"><h3>事業紹介 </h3></div>
            </a>
        </li>
        <li class="c-list4__card ">
            <a href="/recruit/company/business/advantage.php" class="c-arrow">
                <div class="c-list4__img" style="background:url('/recruit/assets/img/company-index/104.jpg') center center; background-size:cover;">
                </div>
                <div class="c-list4__text"><h3>JXTG<br class="sp-only"/>グループの<br class="sp-only"/>強み</h3></div>
            </a>
        </li>
        <li class="c-list4__card ">
            <a href="/recruit/company/info/data.php" class="c-arrow">
                <div class="c-list4__img" style="background:url('/recruit/assets/img/company-index/105.jpg') center center; background-size:cover;">
                </div>
                 <div class="c-list4__text"><h3>データで見る<br class="sp-only"/>ENEOS <br class="pc-only"/>
            ジェネ<br class="sp-only"/>レーションズ</h3></div>
            </a>
        </li>
        <li class="c-list4__card ">
            <a href="/recruit/company/info/outline.php" class="c-arrow">
                <div class="c-list4__img" style="background:url('/recruit/assets/img/company-index/106.jpg') center center; background-size:cover;">
                </div>
                <div class="c-list4__text"><h3>会社概要</h3></div>
            </a>
        </li>
        <li class="c-list4__card ">
            <a href="/recruit/company/info/service_station.php" class="c-arrow">
                <div class="c-list4__img" style="background:url('/recruit/assets/img/company-index/107.jpg') center center; background-size:cover;">
                    <img src="/recruit/assets/img/company-index/107.jpg" alt="" class="pc-only">
                </div>
                <div class="c-list4__text"><h3>サービス<br class="sp-only"/>ステーション<br class="sp-only"/>一覧 </h3></div>
            </a>
        </li>
    </ul>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list5</div>
<div class="l-container">
    <ul class="c-list5">
        <li>昇給年2回</li>
        <li>賞与年2回</li>
        <li>社会保険完備<br>（雇用保険、労災保険、健康保険、厚生年金保険）</li>
        <li>福利厚生サービス会員<br>（レジャー、宿泊、レストランなどの割引利用）</li>
        <li>退職金制度</li>
        <li>慶弔見舞金制度</li>
        <li>慶弔休暇制度</li>
        <li>育児休業制度</li>
        <li>介護休業制度</li>
        <li>定年再雇用制度</li>
    </ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list6</div>
<div class="l-cont1">
    <ul class="c-list6">
        <li>
            <a href="#" class="c-list6__cart">
                <div class="c-TimeTtl">
                    <p class="c-TimeTtl__txt">2018.00.00</p>
                    <span class="c-label3">イベント</span>
                </div>
                <p class="c-list6__txt1">新着情報タイトルが入ります。この文章はサンプルです。実際の内容とは異なります。新着情報タイトルが入ります。この文章はサンプルです。実際の内容とは異なります。</p>
            </a>
        </li>
        <li>
            <a href="#" class="c-list6__cart">
                <div class="c-TimeTtl">
                    <p class="c-TimeTtl__txt">2018.00.00</p>
                    <span class="c-label3">説明会</span>
                </div>
                <p class="c-list6__txt1">新着情報タイトルが入ります。この文章はサンプルです。実際の内容とは異なります。新着情報タイトルが入ります。この文章はサンプルです。実際の内容とは異なります。</p>
            </a>
        </li>
    </ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list7</div>
<div class="l-cont1">
    <ol class="c-list7">
        <li><span class="c-list7__number">1.</span>新着情報本文が入ります。この文章はサンプルです。実際の内容とは異なります。新着情報本文が入ります。この文章はサンプルです。</li>
        <li><span class="c-list7__number">2.</span>新着情報本文が入ります。この文章はサンプルです。</li>
        <li><span class="c-list7__number">3.</span>新着情報本文が入ります。この文章はサンプルです。</li>
    </ol>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list8</div>
<div class="l-container">
    <ul class="c-list8">
        <li>
            <a href="#" class="c-cart1">
                <div class="c-cart1__img">
                    <img src="https://placehold.jp/540x304.png" alt="">
                </div>
                <div class="c-cart1__txt">
                    <p class="c-cart1__txt1">募集職種一覧／エントリー</p>
                </div>
            </a>
        </li>
        <li>
            <a href="#" class="c-cart1">
                <div class="c-cart1__img">
                    <img src="https://placehold.jp/540x304.png" alt="">
                </div>
                <div class="c-cart1__txt">
                    <p class="c-cart1__txt1">よくあるご質問</p>
                </div>
            </a>
        </li>
    </ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list10</div>
<div class="c-list10">
    <span class="c-label2">新卒</span>
    <span class="c-label2">中途</span>
    <span class="c-label2">アルバイト・パート</span>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list11</div>
<div class="l-container">
    <ul class="c-list11">
        <li>
            <div class="c-card3">
                <div class="c-card3__img">
                    <img src="https://placehold.jp/346x194.png" alt="">
                </div>
                <div class="c-card3__txt">
                    <h4 class="c-card3__ttl">EneJet</h4>
                    <p class="c-card3__txt1">EneJetは「Smart & Convenient」をコンセプトに持つ、「先進的で、早くて、きれい」かつ「使いやすく、便利」なセルフサービスステーションです。「欲しい」に応えるサービスを、お客様へ提供していただきます。</p>
                </div>
            </div>
        </li>
        <li>
            <div class="c-card3">
                <div class="c-card3__img">
                    <img src="https://placehold.jp/346x194.png" alt="">
                </div>
                <div class="c-card3__txt">
                    <h4 class="c-card3__ttl">EneJet</h4>
                    <p class="c-card3__txt1">EneJetは「Smart & Convenient」をコンセプトに持つ、「先進的で、早くて、きれい」かつ「使いやすく、便利」なセルフサービスステーションです。「欲しい」に応えるサービスを、お客様へ提供していただきます。</p>
                </div>
            </div>
        </li>
    </ul>
</div>