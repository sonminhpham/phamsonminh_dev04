<?php /*========================================
img
================================================*/ ?>
<div class="c-dev-title1">img</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtxt1</div>
<div class="l-container">
    <div class="c-imgtxt1">
      <a href="" class="c-imgtxt1__card c-arrow c-arrow--red">
        <div class="c-imgtxt1__img">
            <img src="/recruit/assets/img/index/106.jpg" alt="">
        </div>
        <div class="c-imgtxt1__txt">
            <h3 class="c-imgtxt1__title1">職種紹介</h3>
            <p class="c-imgtxt1__lead">【100文字程度】職種紹介の説明文が入ります。このテキストはサンプルです。実際の内容とは異なります。このテキストはサンプルです。実際の内容とは異なります。</p>
        </div>
      </a>
    </div>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtxt2</div>
<div class="c-imgtxt2">
    <div class="c-imgtxt2__img ">
        <img src="/recruit/assets/img/people-interview-staff/101.jpg" alt="">
    </div>
  <div class="c-imgtxt2__content">
    <div class="c-imgtxt2__text">
            <h3 class="c-imgtxt2__title"> 仮）キャッチコピー<br class="pc-only">学生時代／入社前の活動、<br class="pc-only"> 中央石油販売との出会い</h3>
            <p class="c-imgtxt2__word">【300文字程度】コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。<span class="u-orange">(強調)このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。</span>このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。</p>
        </div>
  </div>
</div>
<br>
<br>
<br>
<div class="c-imgtxt2 c-imgtxt2--left">
    <div class="c-imgtxt2__img">
        <img src="/recruit/assets/img/people-interview-staff/104.jpg" alt="">
    </div>
    <div class="c-imgtxt2__content">
        <div class="c-imgtxt2__text">
            <h3 class="c-imgtxt2__title"> 仮）キャッチコピー<br class="pc-only">将来展望、成長に<br class="pc-only">必要なものとは、<br class="pc-only">どんな社会人でありたいか</h3>
            <p class="c-imgtxt2__word">【300文字程度】コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。</p>
        </div>
    </div>
</div>
<br>
<br>
<br>
<div class="l-img">
<div class="c-imgtxt2 c-imgtxt2--left c-imgtxt2--small">
    <div class="c-imgtxt2__img">
        <img src="/recruit/assets/img/people-interview-staff/102.jpg" alt="">
    </div>
    <div class="c-imgtxt2__content">
        <div class="c-imgtxt2__text">
            <h3 class="c-imgtxt2__title"> 仮）キャッチコピー<br class="pc-only">将来展望、成長に<br class="pc-only">必要なものとは、<br class="pc-only">どんな社会人でありたいか</h3>
            <p class="c-imgtxt2__word">【300文字程度】コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。</p>
        </div>
    </div>
</div>
<br>
<br>
<br>
<div class="c-imgtxt2 c-imgtxt2--small">
    <div class="c-imgtxt2__img">
        <img src="/recruit/assets/img/people-interview-staff/103.jpg" alt="">
    </div>
  <div class="c-imgtxt2__content">
    <div class="c-imgtxt2__text">
            <h3 class="c-imgtxt2__title"> 仮）キャッチコピー<br>
            学生時代／入社前の活動、<br>
            中央石油販売との出会い</h3>
            <p class="c-imgtxt2__word">【300文字程度】コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。<span class="u-orange">(強調)このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。</span>このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。</p>
        </div>
  </div>
</div>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtxt3</div>
<div class="l-containerMax">
    <div class="c-imgtxt3">
        <div class="c-imgtxt3__bg">
            <div class="c-imgtxt3__img sp-only">
                <!-- <img src="/recruit/assets/img/index/110.png" alt=""> -->
                <div class="c-imgtxt3__img--inner c-imgtxt3__img--company"></div>
            </div>
            <div class="c-imgtxt3__text">
                <h3>お客様の笑顔と、<br>従業員の笑顔。<br>どちらも大切にする会社。</h3>
                <p>私たちはJXTGグループ出資の販売会社として、ガソリンなどの燃料をお客様の手元に届ける役割を担っています。お客様に満足していただけるサービスを提供するために、最も大切な資産は従業員の皆さんです。お客様も従業員も笑顔になれるサービスステーションをつくることが、会社の成長につながると考えています。</p>
                <div class="c-btn2">
                    <a href="" class="c-arrow"><span>企業理念</span></a>
                </div>
            </div>
        </div>
        <div class="c-imgtxt3__img pc-only">
            <!-- <img src="/recruit/assets/img/index/110.png" alt=""> -->
            <div class="c-imgtxt3__img--inner c-imgtxt3__img--company"></div>
        </div>
    </div>
</div>

<br>
<br>
<br>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtxt3--reverse</div>
<div class="l-containerMax">
    <div class="c-imgtxt3 c-imgtxt3--reverse">
        <div class="c-imgtxt3__img">
            <!-- <img src="/recruit/assets/img/index/111.png" alt=""> -->
            <div class="c-imgtxt3__img--inner"></div>
        </div>
        <div class="c-imgtxt3__bg">
            <div class="c-imgtxt3__text">
                <h3>家族に自慢できる会社。</h3>
                <p>中央石油販売では、社員一人ひとりがワークライフバランスを実現し、情熱を持って働けるよう、充実した福利厚生や教育・研修体制を用意しています。また震災発生後にも給油ができる震災対応サービスステーションへの対応など、幅広いCSR活動にも取り組んでいます。働きやすい環境と高い倫理観を持った「家族に自慢できる会社」であり続けるために、私たちは努力を重ねています。</p>
                <div class="c-btn2">
                    <a href="" class="c-arrow"><span>キャリアパス／<br class="sp-only">教育・研修体制 </span></a>
                </div>
            </div>
        </div>
    </div>
    <div class="l-container l-btn c-link">
        <div class="c-btn1 c-btn1--type2">
            <a href="" class="c-btn1__link c-arrow">
            <span class="c-btn1__img"><img src="/recruit/assets/img/common/welfare-red.svg" alt="" width="24" height="24"></span>
            <p class="c-btn1__text">福利厚生／制度 </p>
            </a>
        </div>
        <div class="c-btn1 c-btn1--type2">
            <a href="" class="c-btn1__link c-arrow">
            <span class="c-btn1__img"><img src="/recruit/assets/img/common/smile-red.svg" alt="" width="24" height="22"></span>
            <p class="c-btn1__text">笑顔の体験談</p>
            </a>
        </div>
        <div class="c-btn1 c-btn1--type2">
            <a href="" class="c-btn1__link c-arrow">
            <span class="c-btn1__img"><img src="/recruit/assets/img/common/CSR-red.svg" alt="" width="23" height="24"></span>
            <p class="c-btn1__text">CSR活動</p>
            </a>
        </div>
    </div>
</div>
<br><br><br><br><br><br><br><br><br><br><br><br>
<br><br><br><br><br><br><br><br><br><br><br><br>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtxt4</div>
<div class="l-container">
<div class="c-imgtxt4">
    <div class="c-imgtxt4__txt">
        <div class="c-title8">
            <h4 class="c-title8__main"><span>新卒・中途研修</span></h4>
        </div>
        <p>入社後はまず、会社の概要説明や、「管理」「安全」といった各部門の担当者による説明など、座学を中心とした研修を受けていただきます。その後、数日間かけてサービスステーション、ドトールコーヒーショップ、セブン-イレブンの業務を体験する実地研修を行います。この実地研修を通じてやりたい仕事を見つけていただければ、配属の際に希望を反映します。もちろん、将来的に異動することも可能です。配属先が決定した後、店舗でのOJTを通して深い知識とスキルを身に付けていただきます。</p>
        <p>また店長研修など、階層に応じた研修も行っています。当社ではキャリアごとに必要な知識・スキルを習得できる教育体制を整えています。</p>
    </div>
    <div class="c-imgtxt4__img">
    <img src="/recruit/assets/img/culture-career-growth/101.jpg" alt="">
    </div>
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtxt7</div>
<div class="l-container">
    <div class="c-imgtxt7">
        <div class="c-imgtxt7__txt">
            <h3 class="c-title11">入社時の仕事内容、挫折と成長、上司／先輩とのエピソードについて教えてください。</h3>
            <p class="c-text1">【300文字程度】コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。</p>
        </div>
        <div class="c-imgtxt7__img">
            <img src="/recruit/assets/img/common/img-102.jpg" alt="">
        </div>
    </div>
</div>
