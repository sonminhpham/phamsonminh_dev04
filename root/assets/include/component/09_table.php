<?php /*========================================
table
================================================*/ ?>
<div class="c-dev-title1">table</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-table1</div>
<div class="l-container">
    <table class="c-table1">
        <tr>
            <th>検索条件指定</th>
            <th >
            <a href="" class="c-table1__btn"><span><img src="/recruit/assets/img/common/reload-white.svg" alt="" width="18" height="17"></span>条件リセット</a>
            </th>
        </tr>
        <tr>
            <td class="c-table1__first">
                <span>
                    <img src="/recruit/assets/img/common/man_01-red.svg" alt="" width="22" height="22" class="pc-only">
                    <img src="/recruit/assets/img/common/man_01-red.svg" alt="" width="11" height="11" class="sp-only">
                </span>
                雇用形態</td>
            <td class="c-table1__last">
                <div class="c-input">
                    <input type="checkbox" name="aaa8" checked>
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>新卒</label>
                </div>
                <div class="c-input">
                    <input type="checkbox" name="aaa8">
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>中途</label>
                </div>
                <div class="c-input">
                    <input type="checkbox" name="aaa8">
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>アルバイト・パート</label>
                </div>
            </td>
        </tr>
        <tr>
            <td class="c-table1__first">
                <span>
                    <img src="/recruit/assets/img/common/occupations-red.svg" alt="" width="23" height="21" class="pc-only">
                    <img src="/recruit/assets/img/common/occupations-red.svg" alt="" width="12" height="11" class="sp-only">
                </span>職種</td>
            <td class="c-table1__last">
                <div class="l-checkbox">
                    <div class="c-input">
                        <input type="checkbox" name="aaa8" checked>
                        <img class="svg" src="/recruit/assets/img/common/check.svg">
                        <label>CM<span>（アルバイト）</span></label>
                    </div>
                    <div class="c-input">
                        <input type="checkbox" name="aaa8">
                        <img class="svg" src="/recruit/assets/img/common/check.svg">
                        <label>CM2-CM3<span>（パート）</span></label>
                    </div>
                    <div class="c-input">
                        <input type="checkbox" name="aaa8">
                        <img class="svg" src="/recruit/assets/img/common/check.svg">
                        <label>CMリーダー<span>（パート）</span></label>
                    </div>
                    <div class="c-input">
                        <input type="checkbox" name="aaa8">
                        <img class="svg" src="/recruit/assets/img/common/check.svg">
                        <label>CM店長<span>（パート）</span></label>
                    </div>
                    <div class="c-input">
                        <input type="checkbox" name="aaa8">
                        <img class="svg" src="/recruit/assets/img/common/check.svg">
                        <label>スタッフ</label>
                    </div>
                    <div class="c-input">
                        <input type="checkbox" name="aaa8">
                        <img class="svg" src="/recruit/assets/img/common/check.svg">
                        <label>主任</label>
                    </div>
                    <div class="c-input">
                        <input type="checkbox" name="aaa8">
                        <img class="svg" src="/recruit/assets/img/common/check.svg">
                        <label>店長<span>（マネージャー）</span></label>
                    </div>
                    <div class="c-input">
                        <input type="checkbox" name="aaa8">
                        <img class="svg" src="/recruit/assets/img/common/check.svg">
                        <label>テリトリー<span>マネージャー</span></label>
                    </div>
                    <div class="c-input">
                        <input type="checkbox" name="aaa8">
                        <img class="svg" src="/recruit/assets/img/common/check.svg">
                        <label>エリア<span>マネージャー</span></label>
                    </div>
                    <div class="c-input">
                        <input type="checkbox" name="aaa8">
                        <img class="svg" src="/recruit/assets/img/common/check.svg">
                        <label>スペシャリスト</label>
                    </div>
                    <div class="c-input">
                        <input type="checkbox" name="aaa8">
                        <img class="svg" src="/recruit/assets/img/common/check.svg">
                        <label>シニア<span>スペシャリスト</span></label>
                    </div>
                    <div class="c-input">
                        <input type="checkbox" name="aaa8">
                        <img class="svg" src="/recruit/assets/img/common/check.svg">
                        <label>グループリード</label>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="c-table1__first">
                <span>
                    <img src="/recruit/assets/img/common/calendar-red.svg" alt="" width="22" height="22" class="pc-only">
                    <img src="/recruit/assets/img/common/calendar-red.svg" alt="" width="11" height="11" class="sp-only">
                </span>入社年</td>
            <td class="c-table1__last">
                <div class="c-input">
                    <input type="checkbox" name="aaa8" checked>
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>～2年目</label>
                </div>
                <div class="c-input">
                    <input type="checkbox" name="aaa8">
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>2～5年目</label>
                </div>
                <div class="c-input">
                    <input type="checkbox" name="aaa8">
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>5～8年目</label>
                </div>
                <div class="c-input">
                    <input type="checkbox" name="aaa8">
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>8年以上</label>
                </div>
            </td>
        </tr>
        <tr>
            <td class="c-table1__first">
                <span>
                    <img src="/recruit/assets/img/common/pin-red.svg" alt=""  width="19" height="22" class="pc-only">
                    <img src="/recruit/assets/img/common/pin-red.svg" alt=""  width="10" height="11" class="sp-only">
                </span>勤務地</td>
            <td class="c-table1__last">
                <div class="c-input">
                    <input type="checkbox" name="aaa8" checked>
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>本社</label>
                </div>
                <div class="c-input">
                    <input type="checkbox" name="aaa8">
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>北海道・東北</label>
                </div>
                <div class="c-input">
                    <input type="checkbox" name="aaa8">
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>関東</label>
                </div>
                <div class="c-input">
                    <input type="checkbox" name="aaa8">
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>中部</label>
                </div>
                <div class="c-input">
                    <input type="checkbox" name="aaa8">
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>関西</label>
                </div>
                <div class="c-input">
                    <input type="checkbox" name="aaa8">
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>中国</label>
                </div>
                <div class="c-input">
                    <input type="checkbox" name="aaa8">
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>九州</label>
                </div>
            </td>
        </tr>
        <tr>
            <td class="c-table1__first">
                <span>
                    <img src="/recruit/assets/img/common/gender-red.svg" alt="" width="19" height="22" class="pc-only">
                    <img src="/recruit/assets/img/common/gender-red.svg" alt="" width="10" height="11" class="sp-only">
                </span>性別</td>
            <td class="c-table1__last">
                <div class="c-input">
                    <input type="checkbox" name="aaa8" checked>
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>男</label>
                </div>
                <div class="c-input">
                    <input type="checkbox" name="aaa8">
                    <img class="svg" src="/recruit/assets/img/common/check.svg">
                    <label>女</label>
                </div>
            </td>
        </tr>
        <tr class="sp-only">
            <td><a href="" class="c-table1__btn">
                <span>
                    <img src="/recruit/assets/img/common/reload-white.svg" alt="" width="18" height="17" class="pc-only">
                    <img src="/recruit/assets/img/common/reload-white.svg" alt="" width="13" height="12" class="sp-only">
                </span>条件リセット</a></td>
        </tr>
    </table>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-table2</div>
<div class="l-container">
<table class="c-table2">
    <tr>
        <td class="c-table2__first">06:30</td>
        <td class="c-table2__last">入店、開店準備</td>
    </tr>
    <tr>
        <td class="c-table2__first">07:15</td>
        <td class="c-table2__last">回転、朝ピーク</td>
    </tr>
    <tr>
        <td class="c-table2__first">09:00</td>
        <td class="c-table2__last">メールチェック、前日の売上の確認</td>
    </tr>
    <tr>
        <td class="c-table2__first">12:00</td>
        <td class="c-table2__last">昼ピーク</td>
    </tr>
    <tr>
        <td class="c-table2__first">13:00</td>
        <td class="c-table2__last">翌日以降のシフト確認、スケジュール作成</td>
    </tr>
    <tr>
        <td class="c-table2__first">14:00</td>
        <td class="c-table2__last">事務業務（パートナー面談、時間帯売上の確認など）</td>
    </tr>
    <tr>
        <td class="c-table2__first">15:30</td>
        <td class="c-table2__last">退勤</td>
    </tr>
</table>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-table3</div>
<div class="l-container">
<table class="c-table3">
    <tr>
        <td>勤  務  地</td>
        <td>埼玉</td>
    </tr>
    <tr>
        <td>居  住  地</td>
        <td>東京都内</td>
    </tr>
    <tr>
        <td>家　　賃</td>
        <td>132,000円</td>
    </tr>
    <tr>
        <td>自己負担</td>
        <td><span class="u-orange">10,000円</span></td>
    </tr>
</table>
</div>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-table4</div>
<div class="l-container">
<table class="c-table4">
    <tbody>
    <tr>
        <td>会社名</td>
        <td><p class="c-table4__txt1">中央石油販売株式会社</p></td>
    </tr>
    <tr>
        <td>雇用形態</td>
        <td><p class="c-table4__txt1">正社員</p></td>
    </tr>
    <tr>
        <td>募集職種</td>
        <td><p class="c-table4__txt1">総合職（ストアマネージャー（店長）候補）</p></td>
    </tr>
    <tr>
        <td>求める人材</td>
        <td><p class="c-table4__txt1">当社の評価制度は、個人評価よりも、チーム（店舗/エリア）評価に重点を置いています。</p>
        <p>体育会出身、とりわけチームスポーツに励んできた皆様のチームプレー経験に大きな期待をしています。当社では”お客様を笑顔に”が社員の合言葉ですので、スポーツで成功体験した時のような健康的な笑顔でお客様に対応してもらえる皆様をお待ちしています。</p></td>
    </tr>
    <tr>
        <td>応募資格</td>
        <td><p class="c-table4__txt1">大学院、大学、短大、専門、高専、高卒、中卒、2019年卒業予定者</p></td>
    </tr>
    <tr>
        <td>募集学科</td>
        <td><p class="c-table4__txt1">学部学科不問</p></td>
    </tr>
    <tr>
        <td>予定勤務地</td>
        <td>
            <p class="c-table4__txt1">東北～九州までのサービスステーション</p>
            <p class="c-table4__txt1">宮城県 ／ 福島県 ／ 茨城県 ／ 栃木県 ／ 群馬県 ／ 埼玉県 ／ 千葉県 ／ 東京都 ／ 神奈川県 ／ 静岡県 ／ 愛知県 ／ 三重県 ／ 京都府 ／ 大阪府 ／ 兵庫県 ／ 奈良県 ／ 和歌山県 ／ 広島県 ／ 山口県 ／ 福岡県</p>
            <p class="c-table4__txt2">※グローバル社員（転勤あり）、ホームタウン社員（転勤なし）をご自身で選択頂けます。</p>
    </td>
    </tr>
    <tr>
        <td>勤務時間</td>
        <td>
            <p class="c-table4__txt1">実働8時間</p>
            <p class="c-table4__txt2">※変形労働時間制（週平均40時間以内）</p>
        </td>
    </tr>
    <tr>
        <td>給与</td>
        <td>
        <ul class="c-list5">
            <li>大学院卒・大学卒・自動車整備大学校卒<p>月給19万円+諸手当～</p></li>
            <li>短大卒・専門学校卒・高専卒・高卒・中卒<p>月給18万円+諸手当～</p></li>
        </ul>
        <br>
        <p class="c-table4__txt1"><strong>《モデル月収例》</strong></p>
        <br>
        <p class="c-table4__txt1">平均月給20万円（入社1年、主任職）：年収330万円</p>
        <p class="c-table4__txt1">平均月給35万円（入社3年、ストアマネジャー職）：年収500万円</p>
        <p class="c-table4__txt1">平均月給50万円（入社10年、テリトリマネジャー職）：年収700万円</p>
        </td>
    </tr>
    <tr>
        <td>
        昇給・賞与
        </td>
        <td>
            <p class="c-table4__txt1">昇給：年2回（4月、10月）</p>
            <p class="c-table4__txt1">賞与：年2回（7月、12月）</p>
        </td>
    </tr>
    <tr>
        <td>諸手当</td>
        <td>
            <p class="c-table4__txt1">残業手当、役職手当（10,000円～160,000円）、住宅手当（15,000円～20,000円）、通勤手当（規定あり）、グローバル手当（5,000円～20,000円）、ライフアシスト手当（20,000円）</p>
        </td>
    </tr>
    <tr>
        <td>休日休暇</td>
        <td>
            <p class="c-table4__txt1">週休2日制（月9日）、有休休暇、結婚休暇、忌引休暇、出産休暇、災害休暇、子の看護休暇、育児休業、介護休業</p>
        </td>
    </tr>
    <tr>
        <td>福利厚生</td>
        <td>
            <p class="c-table4__txt1">社会保険完備（雇用、労災、健康、厚生年金）、表彰制度、福利厚生サービス会員（レジャー、宿泊、レストラン等割引利用）、退職金制度、慶弔金・見舞金制度（結婚、出産、香典、傷病見舞、災害見舞）、借上社宅制度（居住期間期限なし）、地域限定社員制度</p>
            <br><p class="c-table4__txt1"><strong>借上社宅制度の例</strong></p><br>
            <ul class="c-list5">
                <li><strong>独身者の場合</strong>
                    <br>勤務地：横浜<br>
                    居住地：神奈川県内<br>
                    家賃：68,000円<br>
                    自己負担額：5,000円<br>
                </li>
                <li><strong>家族4人の場合</strong>
                <br>勤務地：埼玉<br>
                    居住地：東京都内<br>
                    家賃：132,000円<br>
                    自己負担額：10,000円<br>
                </li>
            </ul>
        </td> 
    </tr>
    <tr>
        <td>教育制度</td>
        <td>
            <ul class="c-list5">
                <li><strong>資格・技術研修</strong>
                    <br>自動車整備士、乙種第4類危険物取扱者
                </li>
                <li><strong>スキルアップ研修</strong>
                    <br>コーチングスキル研修 接客サービス研修、販売・技術スキルアップ研修
                </li>
                <li><strong>階層別研修</strong>
                    <br>新入社員研修、新任店長研修、新任ＴＭ研修、安全管理研修
                </li>
            </ul>
        </td>
    </tr>
    <tr>
        <td>選考基準</td>
        <td><p class="c-table4__txt1">面接を重視します。</p></td>
    </tr>
    <tr>
        <td>応募・選考時提出書類</td>
        <td><p class="c-table4__txt1">履歴書</p></td>
    </tr>
    <tr>
        <td>採用予定人数／実績</td>
        <td><p class="c-table4__txt1">今年度予定：10名～20名</p>
        <p class="c-table4__txt1">昨年度実績：0名</p>
        <p class="c-table4__txt2">※昨年度は採用活動未実施につき、採用はしておりません。</p>
    </td>
    </tr>
    <tr>
        <td>試用期間</td>
        <td><p class="c-table4__txt1">あり　6か月</p></td>
    </tr>
    <tr>
        <td>特記事項</td>
        <td>
            <div class="c-list10">
                <span class="c-label2 c-label2--orange">学歴不問</span>
                <span class="c-label2 c-label2--orange">未経験者歓迎</span>
                <span class="c-label2 c-label2--orange">交通費支給</span>
                <span class="c-label2 c-label2--orange">昇給・昇格あり</span>
            </div>
        </td>
    </tr>
</tbody></table>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-table5</div>
<div class="l-cont1">
    <table class="c-table5">
        <tr>
            <th>大項目</th>
            <th>大項目</th>
            <th>大項目</th>
        </tr>
        <tr>
            <td class="c-table5__txt1">中項目</td>
            <td class="c-table5__txt2">小項目</td>
            <td class="c-table5__txt2">小項目</td>
        </tr>
        <tr>
            <td class="c-table5__txt1">中項目</td>
            <td class="c-table5__txt2">小項目</td>
            <td class="c-table5__txt2">小項目</td>
        </tr>
    </table>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-table6</div>
<div class="l-container">
<table class="c-table6">
        <tbody>
            <tr>
                <td>泉インター</td>
                <td>
                    <p>〒981-3137</p>
                    <p>宮城県仙台市泉区大沢1-1-1</p>
                    <p>TEL：022-776-6109</p>
                    <div class="c-label1">
                        <span class="c-label1__box">給油方式</span>
                        <span class="c-label1__text"> SELF</span></div>
                    <div class="c-label1">
                        <span class="c-label1__box">営業時間</span> 
                        <span class="c-label1__text">24時間</span>
                    </div>
                    <div class="c-label1">
                        <span class="c-label1__box">営業時間</span> 
                        <span class="c-label1__text"> 無休</span>
                    </div>
                    <br>
                    <span class="c-label2">シナジーカード</span>
                    <span class="c-label2">スピードパス</span>
                    <span class="c-label2">スピードパスプラス</span>
                    <span class="c-label2">コーポレートカード</span>
                    <span class="c-label2">エクスプレスウォッシュ</span>
                    <span class="c-label2">車検</span>
                    <span class="c-label2">リペア</span>
                    <span class="c-label2">カーコーティング</span>
                    <span class="c-label2">オイル交換</span>
                    <span class="c-label2">自動車保険</span>
                    <a href="#" class="c-link1">詳細はこちら</a>
                </td>
            </tr>
            <tr>
                <td>名取中央</td>
                <td>
                    <p>〒981-1225</p>
                    <p>宮城県名取市飯野坂土府202-1</p>
                    <p>TEL：022-382-2085</p>
                    <div class="c-label1">
                        <span class="c-label1__box">給油方式</span>
                        <span class="c-label1__text"> SELF</span></div>
                    <div class="c-label1">
                        <span class="c-label1__box">営業時間</span> 
                        <span class="c-label1__text">24時間</span>
                    </div>
                    <div class="c-label1">
                        <span class="c-label1__box">営業時間</span> 
                        <span class="c-label1__text"> 無休</span>
                    </div>
                    <br>
                    <span class="c-label2">シナジーカード</span>
                    <span class="c-label2">スピードパス</span>
                    <span class="c-label2">スピードパスプラス</span>
                    <span class="c-label2">コーポレートカード</span>
                    <a href="#" class="c-link1">詳細はこちら</a>
                </td>
            </tr>
            <tr>
                <td>上飯田</td>
                <td>
                    <p>〒984-0838</p>
                    <p>宮城県仙台市若林区上飯田字天神48</p>
                    <p>TEL：022-289-5757</p>
                    <div class="c-label1">
                        <span class="c-label1__box">給油方式</span>
                        <span class="c-label1__text"> SELF</span></div>
                    <div class="c-label1">
                        <span class="c-label1__box">営業時間</span> 
                        <span class="c-label1__text">24時間</span>
                    </div>
                    <div class="c-label1">
                        <span class="c-label1__box">営業時間</span> 
                        <span class="c-label1__text"> 無休</span>
                    </div>
                    <br>
                    <span class="c-label2">シナジーカード</span>
                    <span class="c-label2">スピードパス</span>
                    <span class="c-label2">スピードパスプラス</span>
                    <span class="c-label2">コーポレートカード</span>
                    <a href="#" class="c-link1">詳細はこちら</a>
                </td>
            </tr>
        </tbody>
    </table>
</div>