<?php /*========================================
title
================================================*/ ?>
<div class="c-dev-title1">title</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title1</div>

<div class="c-title1">
    <h2 class="c-title1__main">Concept</h2>
    <span class="c-title1__sub">採用コンセプト</span>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title2</div>
<div class="c-title2">
    <h3 class="c-title2__main">Interview</h3>
    <p class="c-title2__sub">社員インタビュー</p>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title3</div>
<div class="c-title3">
    <h3 class="c-title3__main">最初の1分に <span>情熱を。</span></h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title3--small</div>
<div class="c-title3 c-title3--small">
    <h3 class="c-title3__main">最初の1分に <span>情熱を。</span></h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title4</div>
<div class="c-title4">
    <h3 class="c-title4__main">Special Interview</h3>
    <p class="c-title4__sub">スペシャルインタビュー</p>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title6</div>
<div class="c-title6">
    <h2 class="c-title6__main">Recruit</h2>
    <p class="c-title6__sub">採用情報</p>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title7</div>
<div class="c-title7">
    <h3 class="c-title7__main">応募職種・勤務地</h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title8</div>
<div class="c-title8">
    <h4 class="c-title8__main"><span>宮城県</span></h4>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title8--orange</div>
<div class="c-title8 c-title8--orange">
    <h4 class="c-title8__main"><span>宮城県</span></h4>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title8--blue</div>
<div class="c-title8 c-title8--blue">
    <h4 class="c-title8__main"><span>宮城県</span></h4>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title9</div>
<div class="l-cont1">
    <h2 class="c-title9">h2新着情報タイトルが入ります。この文章はサンプルです。実際の内容とは異なります。新着情報タイトルが入り<br>ます。(32px/行間48px/#000000)</h2>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title10 (font-size: 3.2rem)</div>
<h3 class="c-title10">仮）キャッチコピー<br>中央石油販売のこれまで。</h3>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title11</div>
<h3 class="c-title11">学生時代／入社前の活動、中央石油販売との出会いについて教えてください。</h3>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title12</div>
<div class="c-title12">
    <h2 class="c-title12__txt1">News</h2>
    <p class="c-title12__txt2">新着情報</p>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title13</div>
<h5 class="c-title13">h5見出しスタイル。この文章はサンプルです。(20px/行間30px/#000000)</h5>