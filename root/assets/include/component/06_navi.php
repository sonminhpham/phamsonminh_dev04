<?php /*========================================
navi
================================================*/ ?>
<div class="c-dev-title1">navi</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi1</div>
<div class="l-container">
    <div class="c-navi1">
        <div class="c-navi1__card">
            <div class="c-navi1__top">
                <h4 class="c-navi1__title">スペシャリスト</h4>
                <span class="c-navi1__box">本社部門</span>
            </div>
            <div class="c-navi1__txt">
                一般社員～主任クラス
            </div>
            <p class="c-navi1__title-list">必要スキル</p>
            <ul class="c-list5 c-list5--small">
                <li>担当業務専門知識</li>
                <li>年度計画の理解と実践</li>
            </ul>
        </div>
        <div class="c-navi1__card">
            <div class="c-navi1__top">
                <h4 class="c-navi1__title">スペシャリスト</h4>
                <span class="c-navi1__box">本社部門</span>
            </div>
            <div class="c-navi1__txt">
                一般社員～主任クラス
            </div>
            <p class="c-navi1__title-list">必要スキル</p>
            <ul class="c-list5 c-list5--small">
                <li>担当業務専門知識</li>
                <li>年度計画の理解と実践</li>
            </ul>
        </div>
        <div class="c-navi1__card">
            <div class="c-navi1__top">
                <h4 class="c-navi1__title">スペシャリスト</h4>
                <span class="c-navi1__box">本社部門</span>
            </div>
            <div class="c-navi1__txt">
                一般社員～主任クラス
            </div>
            <p class="c-navi1__title-list">必要スキル</p>
            <ul class="c-list5 c-list5--small">
                <li>担当業務専門知識</li>
                <li>年度計画の理解と実践</li>
            </ul>
        </div>
    </div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi1--small</div>
<div class="l-container">
 <div class="c-navi1 c-navi1--small">
    <div class="c-navi1__card">
        <p class="c-navi1__title-list">資格・技術研修</p>
        <ul class="c-list5 ">
            <li>国家自動車整備士</li>
            <li>危険物取扱者乙種第4類</li>
            <li>自動車検査員資格</li>
            <li>車検関連</li>
            <li>オイル</li>
            <li>タイヤ</li>
            <li>コーティング</li>
        </ul>
    </div>
    <div class="c-navi1__card">
        <p class="c-navi1__title-list">階層別研修</p>
        <ul class="c-list5">
            <li>新入社員研修</li>
            <li>新任店長研修</li>
            <li>新任TM研修</li>
            <li>安全管理研修</li>
        </ul>
    </div>
    <div class="c-navi1__card">
        <p class="c-navi1__title-list">スキルアップ研修</p>
        <ul class="c-list5">
            <li>コーチングスキル研修</li>
            <li>接客サービス研修</li>
            <li>販売・技術スキルアップ研修</li>
        </ul>
    </div>
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi1--orange</div>
<div class="l-container">
    <div class="c-navi1 c-navi1--orange">
        <div class="c-navi1__card">
            <div class="c-navi1__top">
                <h4 class="c-navi1__title">スペシャリスト</h4>
                <span class="c-navi1__box">本社部門</span>
            </div>
            <div class="c-navi1__txt">
                一般社員～主任クラス
            </div>
            <p class="c-navi1__title-list">必要スキル</p>
            <ul class="c-list5 c-list5--small">
                <li>担当業務専門知識</li>
                <li>年度計画の理解と実践</li>
            </ul>
        </div>
        <div class="c-navi1__card">
            <div class="c-navi1__top">
                <h4 class="c-navi1__title">スペシャリスト</h4>
                <span class="c-navi1__box">本社部門</span>
            </div>
            <div class="c-navi1__txt">
                一般社員～主任クラス
            </div>
            <p class="c-navi1__title-list">必要スキル</p>
            <ul class="c-list5 c-list5--small">
                <li>担当業務専門知識</li>
                <li>年度計画の理解と実践</li>
            </ul>
        </div>
        <div class="c-navi1__card">
            <div class="c-navi1__top">
                <h4 class="c-navi1__title">スペシャリスト</h4>
                <span class="c-navi1__box">本社部門</span>
            </div>
            <div class="c-navi1__txt">
                一般社員～主任クラス
            </div>
            <p class="c-navi1__title-list">必要スキル</p>
            <ul class="c-list5 c-list5--small">
                <li>担当業務専門知識</li>
                <li>年度計画の理解と実践</li>
            </ul>
        </div>
        <div class="c-navi1__card">
            <div class="c-navi1__top">
                <h4 class="c-navi1__title">スペシャリスト</h4>
                <span class="c-navi1__box">本社部門</span>
            </div>
            <div class="c-navi1__txt">
                一般社員～主任クラス
            </div>
            <p class="c-navi1__title-list">必要スキル</p>
            <ul class="c-list5 c-list5--small">
                <li>担当業務専門知識</li>
                <li>年度計画の理解と実践</li>
            </ul>
        </div>
    </div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi1--blue</div>
<div class="l-container">
    <div class="c-navi1 c-navi1--blue">
        <div class="c-navi1__card">
            <div class="c-navi1__top">
                <h4 class="c-navi1__title">スペシャリスト<span>（アルバイト）</span></h4>
                <span class="c-navi1__box">本社部門</span>
            </div>
            <div class="c-navi1__txt">
                一般社員～主任クラス
            </div>
            <p class="c-navi1__title-list">必要スキル</p>
            <ul class="c-list5 c-list5--small">
                <li>担当業務専門知識</li>
                <li>年度計画の理解と実践</li>
            </ul>
        </div>
        <div class="c-navi1__card">
            <div class="c-navi1__top">
                <h4 class="c-navi1__title">スペシャリスト</h4>
                <span class="c-navi1__box">本社部門</span>
            </div>
            <div class="c-navi1__txt">
                一般社員～主任クラス
            </div>
            <p class="c-navi1__title-list">必要スキル</p>
            <ul class="c-list5 c-list5--small">
                <li>担当業務専門知識</li>
                <li>年度計画の理解と実践</li>
            </ul>
        </div>
        <div class="c-navi1__card">
            <div class="c-navi1__top">
                <h4 class="c-navi1__title">スペシャリスト</h4>
                <span class="c-navi1__box">本社部門</span>
            </div>
            <div class="c-navi1__txt">
                一般社員～主任クラス
            </div>
            <p class="c-navi1__title-list">必要スキル</p>
            <ul class="c-list5 c-list5--small">
                <li>担当業務専門知識</li>
                <li>年度計画の理解と実践</li>
            </ul>
        </div>
        <div class="c-navi1__card">
            <div class="c-navi1__top">
                <h4 class="c-navi1__title">スペシャリスト</h4>
                <span class="c-navi1__box">本社部門</span>
            </div>
            <div class="c-navi1__txt">
                一般社員～主任クラス
            </div>
            <p class="c-navi1__title-list">必要スキル</p>
            <ul class="c-list5 c-list5--small">
                <li>担当業務専門知識</li>
                <li>年度計画の理解と実践</li>
            </ul>
        </div>
    </div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi2</div>
<div class="l-container">
    <dl class="c-navi2">
        <dt class="c-navi2__left">
        山 田
        </dt>
        <dd class="c-navi2__right">
        【300文字程度対談形式】コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。
        </dd>
    </dl>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi2--green</div>
<div class="l-container">
    <dl class="c-navi2 c-navi2--green">
        <dt class="c-navi2__left">
        鈴 木
        </dt>
        <dd class="c-navi2__right">
        コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。
        </dd>
    </dl>
</div>


<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi3</div>
<div class="l-container">
    <ul class="c-navi3">
        <li class="c-navi3__card">
            <h3 class="c-navi3__title">スタッフ</h3>
            <p class="c-navi3__label"> 一般社員</p>
            <p class="c-navi3__text">入社</p>
        </li>
        <li class="c-navi3__card">
            <h3 class="c-navi3__title">主任</h3>
            <p class="c-navi3__label">大規模SS店舗の店長補佐</p>
            <p class="c-navi3__text">早ければ1.5年で昇格。</p>
        </li>
        <li class="c-navi3__card">
            <h3 class="c-navi3__title">店長<span>（マネージャー）</span></h3>
            <p class="c-navi3__label"> SS店舗の運営責任者</p>
            <p class="c-navi3__text">早ければ3年で約20名を<br>指揮する責任者に。</p>
        </li>
        <li class="c-navi3__card">
            <h3 class="c-navi3__title">TM<span>（テリトリーマネージャー）</span></h3>
            <p class="c-navi3__label"> テリトリーエリアの責任者</p>
            <p class="c-navi3__text">10～15店舗が所属する<br>テリトリーを統括。</p>
        </li>
        <li class="c-navi3__card">
            <h3 class="c-navi3__title">AM <span>（エリアマネージャー）</span>
            </h3>
            <p class="c-navi3__label"> エリアの総責任者</p>
            <p class="c-navi3__text">4～7のテリトリーを<br>統括。</p>
        </li>
    </ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi4</div>
<div class="l-cont1">
    <ul class="c-navi4">
        <li><a href="#" class="c-navi4__txt c-navi4__back"><span class="c-btnback"></span></a></li>
        <li><a href="#" class="c-navi4__txt">1</a></li>
        <li><span class="c-navi4__more">...</span></li>
        <li><a href="#" class="c-navi4__txt">3</a></li>
        <li><a href="#" class="c-navi4__txt">4</a></li>
        <li><a href="#" class="c-navi4__txt nav4-active">5</a></li>
        <li><a href="#" class="c-navi4__txt">6</a></li>
        <li><a href="#" class="c-navi4__txt">7</a></li>
        <li><span class="c-navi4__more">...</span></li>
        <li><a href="#" class="c-navi4__txt">10</a></li>
        <li><a href="#" class="c-navi4__txt c-navi4__next"><span class="c-btnnext"></span></a></li>
    </ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi5</div>
<div class="l-container">
    <ul class="c-navi5">
        <li>
            <div class="c-cart2">
                <div class="c-cart2__label">
                    <span class="c-label4">STEP 01</span>
                </div>
                <div class="c-cart2__img">
                    <img src="/recruit/assets/img/common/icon-201.png" alt="">
                </div>
                <div class="c-cart2__txt">
                    <p class="c-cart2__txt1">エントリー</p>
                    <p class="c-cart2__txt2">採用媒体より応募</p>
                </div>
            </div>
        </li>
        <li>
            <div class="c-cart2">
                <div class="c-cart2__label">
                    <span class="c-label4">STEP 02</span>
                </div>
                <div class="c-cart2__img">
                    <img src="/recruit/assets/img/common/icon-202.png" alt="">
                </div>
                <div class="c-cart2__txt">
                    <p class="c-cart2__txt1">説明会</p>
                    <p class="c-cart2__txt2">書類選考と1次面接も同時に実施</p>
                </div>
            </div>
        </li>
        <li>
            <div class="c-cart2">
                <div class="c-cart2__label">
                    <span class="c-label4">STEP 03</span>
                </div>
                <div class="c-cart2__img">
                    <img src="/recruit/assets/img/common/icon-203.png" alt="">
                </div>
                <div class="c-cart2__txt">
                    <p class="c-cart2__txt1">役員面接・適性検査</p>
                    <p class="c-cart2__txt2">性格診断、IQ診断を実施</p>
                </div>
            </div>
        </li>
        <li>
            <div class="c-cart2">
                <div class="c-cart2__label">
                    <span class="c-label4">STEP 04</span>
                </div>
                <div class="c-cart2__img">
                    <img src="/recruit/assets/img/common/icon-204.png" alt="">
                </div>
                <div class="c-cart2__txt">
                    <p class="c-cart2__txt1">内定</p>
                    <p class="c-cart2__txt2">エントリーから内定まで<br class="pc-only">約2週間～1ヶ月間</p>
                </div>
            </div>
        </li>
    </ul>
</div>