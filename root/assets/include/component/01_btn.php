<?php /*========================================
btn
================================================*/ ?>
<div class="c-dev-title1">btn</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn1</div>
<div class="l-container">
    <div class="c-btn1">
        <a href="" class="c-btn1__link c-arrow">
        <span class="c-btn1__img"><img class="svg" src="/recruit/assets/img/common/company-red.svg" alt="" width="22" height="24"></span>
        <p class="c-btn1__text">会社概要</p>
        </a>
    </div>
    <br>
    <div class="c-btn1">
        <a href="" class="c-btn1__link  c-arrow">
        <span class="c-btn1__img"><img src="/recruit/assets/img/common/SS-red.svg" alt="" width="24" height="24"></span>
        <p class="c-btn1__text">サービスステーション一覧</p>
        </a>
    </div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn1--type2</div>
<div class="c-btn1 c-btn1--type2">
    <a href="" class="c-btn1__link  c-arrow">
    <span class="c-btn1__img"><img src="/recruit/assets/img/common/welfare-red.svg" alt="" width="24" height="24"></span>
    <p class="c-btn1__text">福利厚生／制度 </p>
    </a>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn1--color</div>
<div class="c-btn1 c-btn1--type2 c-btn1--color">
    <a href="" class="c-btn1__link  c-arrow c-arrow--red">
    <p class="c-btn1__text">新卒採用情報<span>募集職種・募集要項</span></p>
    </a>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">l-btn</div>
<div class="l-container l-btn">
    <div class="c-btn1">
        <a href="" class="c-btn1__link  c-arrow">
        <span class="c-btn1__img"><img src="/recruit/assets/img/common/company-red.svg" alt="" width="22" height="24"></span>
        <p class="c-btn1__text">会社概要</p>
        </a>
    </div>
    <br>
    <div class="c-btn1">
        <a href="" class="c-btn1__link  c-arrow">
        <span class="c-btn1__img"><img src="/recruit/assets/img/common/SS-red.svg" alt="" width="24" height="24"></span>
        <p class="c-btn1__text">サービスステーション一覧</p>
        </a>
    </div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn2</div>
<div class="c-btn2">
    <a href=""class="c-arrow"><span>企業理念</span></a>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn2--type2</div>
<div class="c-btn2 c-btn2--type2">
    <a href=""class="c-arrow c-arrow--red"><span>企業理念</span></a>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn4</div>
<div class="c-btn4">
    <a href="#" class="c-btn4__txt">新着情報一覧</a>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-btn5</div>
<div class="c-btn5">
    <a href="#" class="c-btn5__txt">エントリーフォーム</a>
</div>

