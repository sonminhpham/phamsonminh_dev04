<?php /*========================================
other
================================================*/ ?>
<div class="c-dev-title1">other</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-mainvisual</div>
<!-- <div class="c-staffvisual1">
    <div class="c-staffvisual1__inner1">
        <div class="c-staffvisual1__img">
            <img src="/recruit/assets/img/people-index/100.png" class="pc-only" alt="">
            <img src="/recruit/assets/img/people-interview-staff/100sp.png" class="sp-only" alt="">
            <div class="c-staffvisual1__bg"></div>
        </div>
    </div>
    <div class="c-staffvisual1__title">
        <div class="c-title6">
            <h2 class="c-title6__main">Requirements</h2>
            <p class="c-title6__sub">新卒採用 募集要項／エントリー</p>
        </div>
    </div>
</div> -->

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-mainvisual2</div>
<div class="c-staffvisual2">
    <div class="c-staffvisual2__inner1">
        <div class="c-staffvisual2__img">
            <div class="c-staffvisual2__box">
                <div class="c-staffvisual2__image">
                    <div class="c-staffvisual2__image__cont">
                        <div class="c-staffvisual2__image__bg" style="background: url('/recruit/assets/img/common/mv-204.jpg')no-repeat center; background-size: cover;"></div>
                    </div>
                </div>
            </div>
            <div class="c-staffvisual2__bg"></div>
        </div>
        <div class="c-staffvisual2__text">
            <div class="c-title6">
                <h3 class="c-title6__main">People</h3>
                <p class="c-title6__sub">人を知る</p>
            </div>
        </div>
    </div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-bg</div>
<div style="background:#000; position:relative;height:700px;">
<div class="c-bg">
</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-link1</div>
<a href="#" class="c-link1">詳細はこちら</a>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-label1</div>
 <div class="c-label1">
    <span class="c-label1__box">給油方式</span>
    <span class="c-label1__text"> SELF</span>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-label2</div>
<span class="c-label2">シナジーカード</span>
<span class="c-label2">スピードパス</span>
<span class="c-label2">スピードパスプラス</span>
<span class="c-label2">コーポレートカード</span>
<span class="c-label2">エクスプレスウォッシュ</span>
<span class="c-label2">ドトールコーヒーショップ複合店</span>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-label2--orange</div>
<span class="c-label2 c-label2--orange">シナジーカード</span>
<span class="c-label2 c-label2--orange">シナジーカード</span>
<span class="c-label2 c-label2--orange">シナジーカード</span>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-link-in-page</div>
<div class="l-container">
<ul class="c-link-in-page">
    <li>
        <a href="#">
            <span>本社部門 </span>
        </a>
    </li>
    <li>
        <a href="#">
            <span>販売部門 </span>
        </a>
    </li>
    <li>
        <a href="#">
            <span>アルバイト・パート</span>
        </a>
    </li>
    <li>
        <a href="#">
            <span>アルバイト・パート</span>
        </a>
    </li>
    <li>
        <a href="#">
            <span>アルバイト・パート</span>
        </a>
    </li>
</ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-link-in-page--col3 </div>
<div class="l-container">
<ul class="c-link-in-page c-link-in-page--col3">
    <li>
        <a href="#">
            <span>本社部門 </span>
        </a>
    </li>
    <li>
        <a href="#">
            <span>販売部門 </span>
        </a>
    </li>
    <li>
        <a href="#">
            <span>アルバイト・パート</span>
        </a>
    </li>
</ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-link-in-page--col2 </div>
<div class="l-container">
<ul class="c-link-in-page c-link-in-page--col2">
    <li>
        <a href="#">
            <span>本社部門 </span>
        </a>
    </li>
    <li>
        <a href="#">
            <span>販売部門 </span>
        </a>
    </li>
</ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-faq </div>
<div class="l-container">
    <dl class="c-faq">
        <dt class="c-faq__question">
            <span class="c-faq__label">
            新卒｜中途｜アルバイト・パート
            </span>
            <h3 class="c-faq__title">
            応募資格はありますか？学科や選考の重視はありますか？
            </h3>
        </dt>
        <dd class="c-faq__answer">
            <p>学歴・学部・学科は問いません。入社後は配属先の業態に特化した研修を行いますので、経験や資格が無くても構いません。</p>
            <p>「人と接することが好き」「マネジメントに興味がある」「心機一転、新しい職種にチャレンジしてみたい」などなど、少しでも興味を持った方、お気軽にエントリーください。たくさんの方とお会いできるのを楽しみにしております。</p>
        </dd>
    </dl>
</div>
<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-label3</div>
<span class="c-label3">イベント</span>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-label4</div>
<span class="c-label4">STEP 01</span>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-TimeTtl</div>
<div class="c-TimeTtl">
    <p class="c-TimeTtl__txt">2018.00.00</p>
    <span class="c-label3">イベント</span>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-cart1</div>
<a href="#" class="c-cart1">
    <div class="c-cart1__img">
        <img src="https://placehold.jp/360x270.png" alt="">
    </div>
    <div class="c-cart1__txt">
        <p class="c-cart1__txt1">よくあるご質問</p>
    </div>
</a>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-cart2</div>
<div class="c-cart2">
    <div class="c-cart2__label">
        <span class="c-label4">STEP 01</span>
    </div>
    <div class="c-cart2__img">
        <img src="/recruit/assets/img/common/icon-201.png" alt="">
    </div>
    <div class="c-cart2__txt">
        <p class="c-cart2__txt1">エントリー</p>
        <p class="c-cart2__txt2">採用媒体より応募</p>
    </div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-cart3</div>
<div class="c-card3">
    <div class="c-card3__img">
        <img src="https://placehold.jp/346x194.png" alt="">
    </div>
    <div class="c-card3__txt">
        <h4 class="c-card3__ttl">EneJet</h4>
        <p class="c-card3__txt1">EneJetは「Smart & Convenient」をコンセプトに持つ、「先進的で、早くて、きれい」かつ「使いやすく、便利」なセルフサービスステーションです。「欲しい」に応えるサービスを、お客様へ提供していただきます。</p>
    </div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-cart4</div>
<div class="l-container">
    <div class="c-cart4">
        <div class="c-cart4__left">
            <p class="c-text2">「エッソ・モービル・ゼネラル」は2019年7月までに順次「ENEOS」に変わります。
            セルフサービスステーションブランドのエクスプレス（Express）は、2018年10月以降、コンセプトを引き継いだ新ブランド「EneJet」にリニューアルいたします。</p>
        </div>
        <div class="c-cart4__right">
            <ul class="c-cart4__list">
                <li class="c-cart4__list__icon"><img src="/recruit/assets/img/company-business-business/250.png" alt=""></li>
                <li><img src="/recruit/assets/img/company-business-business/251.png" alt=""></li>
                <li class="c-cart4__list__icon"><img src="/recruit/assets/img/company-business-business/252.png" alt=""></li>
                <li><img src="/recruit/assets/img/company-business-business/253.png" alt=""></li>
            </ul>
        </div>
    </div>
</div>