<?php /*========================================
form
================================================*/ ?>
<div class="c-dev-title1">form</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-form-step</div>
<div class="l-container">
    <ul class="c-form-step">
        <li class="active"><p><span>STEP1</span>情報入力</p></li>
        <li ><p><span>STEP2</span>内容確認</p></li>
        <li><p><span>STEP3</span>応募完了</p></li>
    </ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title7</div>
<div class="c-title7">
    <h3 class="c-title7__main">応募職種・勤務地</h3>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-form-required</div>
<abbr title="" class="c-form-required">必須</abbr>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-form-btn</div>

<button type="submit" class="c-form-btn">入力内容確認</button>
    

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-form__btngroup</div>
<div class="c-form__btngroup">
    <button type="submit" class="c-form-btn c-form-btn--black">修正する</button>
    <button type="submit" class="c-form-btn">送信する</button>
    
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-form-job</div>
<div class="l-container">
    <div class="c-form-job">
        <h3 class="c-form-job__title">
        ガソリンスタンドとドトールの複合店店舗スタッフ
        </h3>
        <div class="c-form-job__table">
            <table>
                <tbody>
                    <tr>
                        <td>雇用形態</td>
                        <td>正社員</td>
                        <td>職種</td>
                        <td>総合職（ストアマネージャー（店長）候補）</td>
                    </tr>
                    <tr>
                        <td>給与</td>
                        <td>月給 19万円</td>
                        <td>勤務地</td>
                        <td>静岡市 曲金</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="c-form-job__btn">
            <span>未経験者歓迎</span>
            <span>交通費支給</span>
            <span>昇給・昇格あり</span>
        </div>
    </div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-form__content</div>
<div class="l-container">
    <div>
        <form action="">
            <fieldset>
                <div class="c-form__row">
                    <div class="c-form__colleft">
                        <label for="">姓名</label>
                        <abbr title="" class="c-required">必須</abbr>
                    </div>
                    <div class="c-form__colright">
                        <label for="">姓</label>
                        <input type="text" class="c-form__name" placeholder="例）山田">
                        <label for="">名</label>
                        <input type="text" class="c-form__name" placeholder="例）太郎">
                        <p class="c-form__placeholder">
                        ※全角文字で入力。
                        </p>
                        <p class="c-form__error">連絡用電話番号を入力してください。</p>
                    </div>
                </div>
                <div class="c-form__row">
                    <div class="c-form__colleft">
                        <label for="">ふりがな</label>
                        <abbr title="" class="c-required">必須</abbr>
                    </div>
                    <div class="c-form__colright">
                        <label for="">せい</label>
                        <input type="text" class="c-form__name" placeholder="例）やまだ">
                        <label for="">めい</label>
                        <input type="text" class="c-form__name" placeholder="例）たろう">
                        <p class="c-form__placeholder">
                        ※全角文字で入力。
                        </p>
                    </div>
                </div>
                <div class="c-form__row">
                    <div class="c-form__colleft">
                        <label for="">性別</label>
                        <abbr title="" class="c-required">必須</abbr>
                    </div>
                    <div class="c-form__colright">
                        <div class="c-form__radio">
                           <div class="c-radio">
                                <input type="radio" name="gender"  checked>
                                <label for="">男</label>
                           </div>
                           <div class="c-radio">
                                <input type="radio" name="gender">
                                <label for="">女</label>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="c-form__row">
                    <div class="c-form__colleft">
                        <label for="">生年月日</label>
                        <abbr title="" class="c-required">必須</abbr>
                    </div>
                    <div class="c-form__colright">
                        <input type="text" name="birthday" id="" class="c-form__birthday" placeholder="例）1900">
                        <label for="">年</label>
                        <input type="text" name="birthday" id="" class="c-form__birthday" placeholder="例）1">
                        <label for="">年</label>
                        <input type="text" name="birthday" id="" class="c-form__birthday" placeholder="例）1">
                        <label for="">年</label>
                    </div>
                </div>
                <div class="c-form__row">
                    <div class="c-form__colleft">
                        <label for="">連絡用メールアドレス</label>
                        <abbr title="" class="c-required">必須</abbr>
                    </div>
                    <div class="c-form__colright">
                        <input type="text" name="email" id="" class="c-form__email" placeholder="例）sample@example.com">
                        <p class="c-form__placeholder">※半角英数字で入力。</p>
                        <label for="">確認用</label>
                        <input type="text" name="email-confirm" id="" class="c-form__email-confirm" placeholder="例）sample@example.com">
                    </div>
                </div>
                <div class="c-form__row">
                    <div class="c-form__colleft">
                        <label for="">郵便番号</label>
                        <abbr title="" class="c-required">必須</abbr>
                    </div>
                    <div class="c-form__colright">
                        <input type="text" name="zip" id="" class="c-form__zip" placeholder="例）1119999">
                        <p class="c-form__placeholder">※半角数字で入力。スペース、ハイフン不要。</p>
                    </div>
                </div>
                <div class="c-form__row">
                    <div class="c-form__colleft">
                        <label for="">都道府県</label>
                        <abbr title="" class="c-required">必須</abbr>
                    </div>
                    <div class="c-form__colright">
                       <select name="province" id="">
                           <option value="">都道府県を選択</option>
                           <option value="">都道府県を選択</option>
                           <option value="">都道府県を選択</option>
                           <option value="">都道府県を選択</option>
                       </select>
                        <p class="c-form__placeholder">※半角数字で入力。スペース、ハイフン不要。</p>
                    </div>
                </div>
                <div class="c-form__row">
                    <div class="c-form__colleft">
                        <label for="">市区町村</label>
                        <abbr title="" class="c-required">必須</abbr>
                    </div>
                    <div class="c-form__colright">
                      <input type="text" name="" id="" class="c-form__biggest">
                    </div>
                </div>
                <div class="c-form__row">
                    <div class="c-form__colleft">
                        <label for="">市区町村</label>
                        <abbr title="" class="c-required">必須</abbr>
                    </div>
                    <div class="c-form__colright">
                      <input type="text" name="" id="" class="c-form__biggest">
                    </div>
                </div>
                <div class="c-form__row">
                    <div class="c-form__colleft">
                        <label for="">市区町村</label>
                        <abbr title="" class="c-required c-required--non">必須</abbr>
                    </div>
                    <div class="c-form__colright">
                      <input type="text" name="" id="" class="c-form__biggest">
                    </div>
                </div>
                <div class="c-form__row">
                    <div class="c-form__colleft">
                        <label for="">連絡先電話番号</label>
                        <abbr title="" class="c-required">必須</abbr>
                    </div>
                    <div class="c-form__colright">
                      <input type="number" name="phone" id="" class="c-form__phone" placeholder="例）0123456789">
                      <p class="c-form__placeholder">※半角数字で入力。スペース、ハイフン不要。</p>
                    </div>
                </div>
                <div class="c-form__row">
                    <div class="c-form__colleft">
                        <label for="">備考</label>
                        <abbr title="" class="c-required c-required--non">必須</abbr>
                    </div>
                    <div class="c-form__colright">
                      <label for="">ご質問・ご要望等ございましたら、こちらに記入をお願いします。</label>
                      <textarea name="" id="" cols="30" rows="10" class="c-form__biggest"></textarea>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-form-privacy</div>
<div class="l-container">
    <div class="c-form-privacy">
        <h2 class="c-form-privacy__title">
        個人情報保護方針
        </h2>
       <div class="c-form-privacy__content">
            <div class="c-form-privacy__text">
                <p>中央石油販売株式会社では、プライバシーの保護が重要であると考え、本ホームページをご覧になられる皆さまからの個人情報やその利用方法について、以下のような方針を持っております。</p>
                <p>1】情報収集とその利用の仕方について<br>皆さまが中央石油販売株式会社のホームページに接続された場合、当社のサーバーを通じ、自動的にページ別の利用頻度等が集計されます。当社はここで集めた情報を統計上の目的に利用させていただき、ホームページの更新・管理に役立てていきますが、「個人情報」として扱われることはありません。</p>
                <p>プライバシーの保護については、名前、住所、電子メールアドレス、さらに電話番号といった、特定の個人もしくは企業を識別できるような個人情報の取扱い管理が重要となります。皆さまが当社のホームページ上で、資料を請求されたり、何か情報を入力される場合、皆さまの個人情報が当社に提供されることになります。当社は皆さまが要望される資料提供等のサービスのためにのみ、こうした個人情報を使用させていただきます。</p>
                <p>「第三者への情報提供」の項で説明いたしますが、第三者の製品やサービスを提供する目的のために当社が「個人情報」を第三者へ提供することはありません。</p>
                <p>fjdsfdsfsfdsfs</p>
                <p>fjdsfdsfsfdsfs</p>
                <p>fjdsfdsfsfdsfs</p>
                <p>fjdsfdsfsfdsfs</p>
            </div>
       </div>
    </div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtxt5</div>
<div class="l-container">
    <div class="c-imgtxt5">
        <div class="c-imgtxt5__txt">
            <h3 class="c-title10">仮）キャッチコピー<br>中央石油販売のこれまで。</h3>
            <p class="c-text1">【300文字程度】コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。<span class="c-text1--orange">（強調）コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。</span>コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。</p>
        </div>
        <div class="c-imgtxt5__img">
            <img src="/recruit/assets/img/common/img-100.jpg" alt="" width="620" height="465">
        </div>
    </div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-imgtxt6</div>
<div class="l-container">
    <div class="c-imgtxt6">
        <div class="c-imgtxt6__img">
            <img src="/recruit/assets/img/common/img-101.jpg" alt="">
        </div>
        <div class="c-imgtxt6__txt">
            <h3 class="c-title10">仮）キャッチコピー<br>中央石油販売のこれから。</h3>
            <p class="c-text1">【300文字程度】コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。コンテンツの説明文が入ります。このテキストはサンプルです。実際の内容とは異なりますので、予めご了承ください。</p>
        </div>
    </div>
</div>